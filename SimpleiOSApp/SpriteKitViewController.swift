//
//  SpriteKitViewController.swift
//  SimpleiOSApp
//
//  Created by Charlie Imhoff on 4/11/15.
//  Copyright (c) 2015 Charlie Imhoff. All rights reserved.
//

import UIKit
import SpriteKit

//An experiment with the built in SpriteKit framework
//
//This view controller grabs its view and uses it to present an SKScene
//The SKScene (BasicScene) renders itself and responds to interaction

class SpriteKitViewController: UIViewController {
	
    override func viewDidLoad() {
        super.viewDidLoad()
		
		if let skView = self.view as? SKView {
			let scene = BasicScene()
			skView.showsFPS = true
			scene.scaleMode = SKSceneScaleMode.ResizeFill
			skView.presentScene(scene)
		}
	}

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

	class BasicScene : SKScene {
		override func didMoveToView(view: SKView) {
			let label = SKLabelNode(fontNamed:"Avenir")
			label.text = "Tap to Drop Blips";
			label.fontSize = 20;
			label.position = CGPoint(x:CGRectGetMidX(self.view!.frame), y:CGRectGetMidY(self.view!.frame));
			self.addChild(label)
			
			let ground = SKShapeNode(rectOfSize: CGSize(width: self.view!.frame.width, height: 10))
			ground.fillColor = SKColor.grayColor()
			ground.position = CGPoint(x: CGRectGetMidX(self.view!.frame), y: CGRectGetMidY(self.view!.frame)-250)
			ground.physicsBody = SKPhysicsBody(rectangleOfSize: CGSize(width: self.view!.frame.width, height: 10))
			ground.physicsBody!.dynamic = false
			self.addChild(ground)
			
			self.physicsWorld.gravity = CGVector(dx: 0, dy: -5)
		}
		
		override func touchesBegan(touches: Set<NSObject>, withEvent event: UIEvent) {
			for touch in (touches as! Set<UITouch>) {
				let location = touch.locationInNode(self)
				
				let ball = SKShapeNode(circleOfRadius: 10)
				
				//select a random color
				let color : SKColor
				let rand = Int(arc4random_uniform(5))
				switch rand {
				case 0:
					color = SKColor.greenColor()
				case 1:
					color = SKColor.redColor()
				case 2:
					color = SKColor.purpleColor()
				case 3:
					color = SKColor.yellowColor()
				case 4:
					color = SKColor.orangeColor()
				default:
					color = SKColor.whiteColor()
				}
				ball.fillColor = color
				
				ball.position = location
				ball.physicsBody = SKPhysicsBody(circleOfRadius: 10)	//physics automatically applied
				
				//sets the ball to wait 10 seconds, then fades the ball out and removes it from the scene
				ball.runAction(SKAction.sequence([SKAction.waitForDuration(10),SKAction.fadeOutWithDuration(0.3),SKAction.removeFromParent()]))
				
				//adds ball to the scene
				self.addChild(ball)
			}
		}
	}
}
