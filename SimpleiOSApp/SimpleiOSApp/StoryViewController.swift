//
//  StoryViewController.swift
//  SimpleiOSApp
//
//  Created by Charlie Imhoff on 4/14/15.
//  Copyright (c) 2015 Charlie Imhoff. All rights reserved.
//

import UIKit

class StoryViewController: UIViewController {

	@IBOutlet var textView : UITextView!
	
	let file = "lists.txt"
	
    override func viewDidLoad() {
        super.viewDidLoad()
		
		if let fileUrl = self.getFileURLForFileInMainBundle(file) {
			textView.text = self.getTextFromFileURL(fileUrl)
		} else {
			textView.text = "File '\(file)' not found in Main Bundle"
		}
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

	private func getTextFromFileURL(url: NSURL) -> String {
		//first read the raw contents...
		if let fileString =  String(contentsOfURL: url, encoding: NSUTF8StringEncoding, error: nil) {
			return fileString
		} else {
			return "Error reading in from url : \(url.description)"
		}
	}

	private func getFileURLForFileInMainBundle(file: String) -> NSURL? {
		let fileName = file.componentsSeparatedByString(".")[0]
		let fileType = file.componentsSeparatedByString(".")[1]
		
		return NSBundle.mainBundle().URLForResource(fileName, withExtension: fileType)
	}
	
}
