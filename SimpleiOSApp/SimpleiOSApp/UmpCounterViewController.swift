//
//  UmpCounterViewController.swift
//  SimpleiOSApp
//
//  Created by Graham Earley on 4/15/15.
//  Copyright (c) 2015 Charlie Imhoff. All rights reserved.
//

import UIKit
import AVFoundation


class UmpCounterViewController: UIViewController {

    @IBOutlet weak var strikesViewContainer: UIView!
    @IBOutlet weak var strikeCountText: UILabel!
    @IBOutlet weak var strikeoutCountText: UILabel!
    @IBOutlet weak var AddStrikeButton: UIButton!

    @IBOutlet weak var ballsViewContainer: UIView!
    @IBOutlet weak var ballCountText: UILabel!
    @IBOutlet weak var walkCountText: UILabel!
    @IBOutlet weak var AddBallButton: UIButton!
    
    @IBOutlet weak var angryUmpImageView: UIImageView!
    
    var umpSoundPlayer = AVAudioPlayer()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        strikeCountText.text = "0"
        strikeoutCountText.text = "0"
        ballCountText.text = "0"
        walkCountText.text = "0"
        
        var alertSound = NSURL(fileURLWithPath: NSBundle.mainBundle().pathForResource("strike3", ofType: "mp3")!)
        
        AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback, error: nil)
        AVAudioSession.sharedInstance().setActive(true, error: nil)
        
        var error:NSError?
        umpSoundPlayer = AVAudioPlayer(contentsOfURL: alertSound, error: &error)
        
    }

    @IBAction func onAddStrikeButton(sender: UIButton) {
        let strikeCount = strikeCountText.text?.toInt()
        
        if let count = strikeCount {
            strikeCountText.text = String(count + 1)
            updateStrikeoutCount()
        }
    }
    
    @IBAction func onAddBallButton(sender: UIButton) {
        let ballCount = ballCountText.text?.toInt()
        
        if let count = ballCount {
            ballCountText.text = String(count + 1)
            updateWalkCount()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func updateStrikeoutCount() {
        let strikeCount = strikeCountText.text?.toInt()
        let strikeoutCount = strikeoutCountText.text?.toInt()
        
        if let count = strikeCount {
            if count == 3 {
                strikeoutCountText.text = String(strikeoutCount! + 1)
                
                // Yell "YERRR OUT" for good measure:
                if umpSoundPlayer.playing {
                    umpSoundPlayer.stop()
                    umpSoundPlayer.currentTime = 0
                }
                umpSoundPlayer.prepareToPlay()
                umpSoundPlayer.play()
                
                resetCount()
            }
        }
        
        // Don't let users get too carried away -- after 5 strikeouts, they need to pay!
        //    (testing alert popups)
        if let strikeouts = strikeoutCount {
            if strikeouts >= 5 {
                var liteAlert = UIAlertController(title: "You're out...", message: "of uses of the strike counter! Buy the pro version on Android if you want to count more than 5 strikeouts at a time!", preferredStyle: UIAlertControllerStyle.Alert)
                
                liteAlert.addAction(UIAlertAction(title: "Oh, okay.", style: .Default, handler: nil))
                
                presentViewController(liteAlert, animated: true, completion: nil)
                
                AddStrikeButton.enabled = false
                if !AddStrikeButton.enabled && !AddBallButton.enabled {
                    replaceAllViewsWithUmpire()
                }
            }
        }
        
    }
    
    func updateWalkCount() {
        let ballCount = ballCountText.text?.toInt()
        let walkCount = walkCountText.text?.toInt()
        
        if let count = ballCount {
            if count == 4 {
                walkCountText.text = String(walkCount! + 1)
                resetCount()
            }
        }
        
        // Again, be mean to users after 5 walks.
        if let walks = walkCount {
            if walks >= 5 {
                var liteAlert = UIAlertController(title: "You're out...", message: "of uses of the walk counter! Buy the pro version on Android if you want to count more than 5 walks at a time!", preferredStyle: UIAlertControllerStyle.Alert)
                
                liteAlert.addAction(UIAlertAction(title: "Oh, okay.", style: .Default, handler: nil))
                
                presentViewController(liteAlert, animated: true, completion: nil)
                
                AddBallButton.enabled = false
                if !AddBallButton.enabled && !AddStrikeButton.enabled {
                    replaceAllViewsWithUmpire()
                }
            }
        }
        
    }
    
    func replaceAllViewsWithUmpire() {
        strikesViewContainer.hidden = true
        ballsViewContainer.hidden = true
        
        angryUmpImageView.hidden = false
        
        UIView.animateWithDuration(3.0, delay: 0.0, options: UIViewAnimationOptions.CurveEaseOut, animations: {
            self.angryUmpImageView.alpha = 1.0
            }, completion: nil)
    }
    
    func resetCount() {
        ballCountText.text = "0"
        strikeCountText.text = "0"
    }
}
