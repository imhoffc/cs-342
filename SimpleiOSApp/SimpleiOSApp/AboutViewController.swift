//
//  AboutViewController.swift
//  SimpleiOSApp
//
//  Created by Graham Earley on 4/21/15.
//  Copyright (c) 2015 Charlie Imhoff. All rights reserved.
//

import UIKit

class AboutViewController: UIViewController {
   
    @IBOutlet weak var aboutTextView: UILabel!
    @IBOutlet weak var secretTapView: UIView!
    
    let tapRecognizer = UITapGestureRecognizer()
    
    // Parallel to initial aboutTextView.text:
    let secretStringArray = ["This", "robot", "is", "equipped", "with", "mind", "control", "and", "laser", "vision.", "You", "are", "being", "hypnotized.", "Nobody", "beats", "robots!"]
    
    var unchangedStringIndices: [Int] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        
        tapRecognizer.addTarget(self, action: "tappedSecretTapView")
        secretTapView.addGestureRecognizer(tapRecognizer)
        
        aboutTextView.text = "This SimpleiOSApp was created by Charlie Imhoff and Graham Earley. Don't tap below here. Nothing will happen."
        
        // Initialize the list of unchanged string indices to include *all* the indices (at first)!
        if let text = aboutTextView.text {
            let maxIndex = (split(text){$0 == " "}).count - 1
            for i in 0...maxIndex {
                unchangedStringIndices.append(i)
            }
        }
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func tappedSecretTapView(){
        let aboutText = aboutTextView.text
        if let text = aboutText {
            let aboutTextArray = split(text) {$0 == " "}
            
            // Ensure that aboutTextArray lines up with secretStringArray
            if (aboutTextArray.count == secretStringArray.count) {
                var newAboutTextArray = [String]()
                // This array will be used to recreate the string at the end of the function call:
                for word in aboutTextArray {
                    newAboutTextArray.append(word)
                }
                
                if !unchangedStringIndices.isEmpty {
                    // Random index (from remaining unchanged indices):
                    let randomUnchangedStringIndex = Int(arc4random_uniform(UInt32(unchangedStringIndices.count)))
                    
                    let index = unchangedStringIndices[randomUnchangedStringIndex]
                    
                    // Remove this index from list of unchanged indices now that it's been changed:
                    unchangedStringIndices.removeAtIndex(randomUnchangedStringIndex)
                    
                    newAboutTextArray[index] = secretStringArray[index]
                    
                    let newAboutText = " ".join(newAboutTextArray)
                    aboutTextView.text = newAboutText
                }
            }
        }
    }
}
