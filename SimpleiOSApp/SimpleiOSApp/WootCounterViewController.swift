//
//  WootCounterViewController.swift
//  SimpleiOSApp
//
//  Created by Charlie Imhoff on 4/19/15.
//  Copyright (c) 2015 Charlie Imhoff. All rights reserved.
//

import UIKit

class WootCounterViewController: UIViewController {

	@IBOutlet weak var wootText: UILabel!
	
	var wootCount = 0
	
    override func viewDidLoad() {
        super.viewDidLoad()
		if let loadedWoots = loadWootCount() {
			wootCount = loadedWoots
		}
		updateWootText()
	}

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()

	}

	@IBAction func onWoot(sender: AnyObject) {
		wootCount++
		updateWootText()
		
		saveWootCountToFile()
	}

	private func updateWootText() {
		if wootCount <= 0 {
			wootText.text = "You've yet to woot!"
		} else {
			var plural = ""
			if wootCount != 1 { plural = "s" }
			wootText.text = "You've wooted \(wootCount) time\(plural)!"
		}
	}
	
	@IBAction func onResetAllWoots(sender: AnyObject) {
		wootCount = 0
		updateWootText()
		
		saveWootCountToFile()
	}
	
	//MARK: - Saving, and Loading
	private let FILE_NAME = "woots.plist"
	private let WOOTS_KEY = "woots"
	
	private func saveWootCountToFile() {
		let savableDictionary = NSDictionary(dictionary: [WOOTS_KEY:wootCount])
		
		if let fileUrl = getWootsFileURL() {
			savableDictionary.writeToURL(fileUrl, atomically: true)
		}
	}
	
	private func loadWootCount() -> Int? {
		if let fileUrl = getWootsFileURL() {
			if let loadedPlist = NSDictionary(contentsOfURL: fileUrl) {
				return loadedPlist.valueForKey(WOOTS_KEY) as? Int
			}
		}
		return nil
	}
	
	
	private func getWootsFileURL() -> NSURL? {
		let directories = NSFileManager.defaultManager().URLsForDirectory(NSSearchPathDirectory.LibraryDirectory, inDomains: NSSearchPathDomainMask.AllDomainsMask)
		if let selectedDirectory = directories[0] as? NSURL {
			let filePath = selectedDirectory.URLByAppendingPathComponent(FILE_NAME)
			return filePath
		} else {
			return nil
		}
	}
	
}
