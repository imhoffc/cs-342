//
//  FormViewController.swift
//  SimpleiOSApp
//
//  Created by Charlie Imhoff on 4/13/15.
//  Copyright (c) 2015 Charlie Imhoff. All rights reserved.
//

import UIKit

class FormViewController: UIViewController, UITextFieldDelegate, UIPickerViewDataSource, UIPickerViewDelegate {

	@IBOutlet var nameField : UITextField!
	@IBOutlet var animalPicker : UIPickerView!
	@IBOutlet var imageView : UIImageView!
	@IBOutlet var imageText : UILabel!
	
	enum PickerAnimal : String {
		case Dog = "Dog"
		case Cat = "Cat"
		case Turtle = "Turtle"
		case Elephant = "Elephant"
		case Dinosaur = "Dinosaur"
	}
	let pickerChoices : [PickerAnimal] = [.Dog, .Cat, .Turtle, .Elephant, .Dinosaur]
	
    override func viewDidLoad() {
        super.viewDidLoad()
		imageText.text = "Get Ready to Meet a New Friend!"
		imageView.image = nil
	}

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

	@IBAction func onMeetNewFriendButton() {
		//options
		let totalDuration : CFTimeInterval = 0.3
		let moveMargin : CGFloat = 10
		let textStartPoint : CGPoint = imageText.layer.position
		
		var fadeOutDuration = totalDuration/2
		if imageView.image == nil {
			//nothing has been picked yet, so no need to animate anything out
			fadeOutDuration = 0
		}
		
		UIView.animateWithDuration(fadeOutDuration, animations: { () -> Void in
			//fade out
			self.imageText.frame.offset(dx: -moveMargin, dy: 0)
			self.imageText.alpha = 0
			self.imageView.frame.offset(dx: -moveMargin, dy: 0)
			self.imageView.alpha = 0
			return
			
		}) { (completionHandler) -> Void in
			//update content while views are hidden
			let selectedIndex = self.animalPicker.selectedRowInComponent(0)
			let selectedAnimal = self.pickerChoices[selectedIndex]
			self.imageText.text = "This is your new buddy, \(self.nameField.text)"
			self.imageView.image = UIImage(named: selectedAnimal.rawValue)
			
			//fade in the same views with new contents
			UIView.animateWithDuration(totalDuration/2, animations: { () -> Void in
				self.imageText.frame.offset(dx: moveMargin, dy: 0)
				self.imageText.alpha = 1
				self.imageView.frame.offset(dx: moveMargin, dy: 0)
				self.imageView.alpha = 1
				return
			})
		}
	}
	
	//MARK: - UITextFieldDelegate
	//(for hiding the keyboard)
	func textFieldShouldReturn(textField: UITextField) -> Bool {
		nameField.resignFirstResponder()
		return true
	}
	
	//MARK: - UIPickerViewDataSource
	
	func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
		return 1
	}
	
	func pickerView(pickerView: UIPickerView,
		numberOfRowsInComponent component: Int) -> Int {
			return pickerChoices.count
	}
	
	//MARK: - UIPickerViewDelegate
	
	func pickerView(pickerView: UIPickerView,
		rowHeightForComponent component: Int) -> CGFloat {
			return 40
	}
	
	func pickerView(pickerView: UIPickerView,
		titleForRow row: Int,
		forComponent component: Int) -> String! {
			return pickerChoices[row].rawValue
	}
	
}
