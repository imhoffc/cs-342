//
//  HomeViewController.swift
//  SimpleiOSApp
//
//  Created by Charlie Imhoff on 4/11/15.
//  Copyright (c) 2015 Charlie Imhoff. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

	// - MARK: Navigation
	enum NavigationItem : String {
		case SpriteKit = "SpriteKit Game"
		case UmpCounter = "Umpire Helper Lite"
		case Form = "Form"
		case Story = "Story"
		case Woot = "Woot"
	}
	let homeItems : [NavigationItem] = [.SpriteKit, .UmpCounter, .Form, .Story, .Woot]
	
	// - MARK: Basic View Control
	override func viewDidLoad() {
		super.viewDidLoad()
	}

	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
		// Dispose of any resources that can be recreated.
	}
	
 	// MARK: - UITableViewDataSource
	let reuseCellID = "homeItemCell"
	
	func numberOfSectionsInTableView(tableView: UITableView) -> Int {
		return 1
	}
	
	func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return self.homeItems.count
	}
	
	func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
		var cell: UITableViewCell! = tableView.dequeueReusableCellWithIdentifier(self.reuseCellID) as? UITableViewCell
		if cell == nil {
			cell = UITableViewCell(style: .Default, reuseIdentifier: self.reuseCellID)
			cell.selectionStyle = UITableViewCellSelectionStyle.None;
			cell.accessoryType = UITableViewCellAccessoryType.DisclosureIndicator;
		}
		
		cell.textLabel?.text = homeItems[indexPath.row].rawValue
		
		return cell
	}
	
	// MARK: - UITableViewDelegate
	
	func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
		return 40
	}
	
	func tableView(tableView: UITableView, willDeselectRowAtIndexPath indexPath: NSIndexPath) -> NSIndexPath? {
		var cell: UITableViewCell? = tableView.cellForRowAtIndexPath(indexPath)
		return indexPath
	}
	
	func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
		let cell = tableView.cellForRowAtIndexPath(indexPath)
		
		if let navSelection = NavigationItem(rawValue: cell?.textLabel?.text ?? "") {
			self.performSegueWithIdentifier(navSelection.rawValue, sender: self)
		}
		
	}
}





