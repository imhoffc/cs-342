package edu.carleton.imhoffc.simpleandroidapp;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import android.widget.Toast;


public class SettingsActivity extends Activity implements AdapterView.OnItemSelectedListener {

    //UI References
    private TextView textView;
    private Spinner colorSelector;
    public static int colorCode = Color.WHITE;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        this.textView = (TextView)this.findViewById(R.id.textView6);
        updateColors();

        this.colorSelector = (Spinner)this.findViewById(R.id.colorSelection);
        //set up spinner with choices
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.colors, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        colorSelector.setAdapter(adapter);
        colorSelector.setOnItemSelectedListener(this);
    }

    private void updateColors() {
        this.textView.setTextColor(colorCode);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_home, menu);
        getMenuInflater().inflate(R.menu.menu_settings, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        // Sliding transition, as said to be used by Jeff
        if (id == android.R.id.home) {
            finish();
            overridePendingTransition(R.anim.slide_in_from_left, R.anim.slide_out_to_right);
            return true;
        } else if (id == R.id.action_about) {
            Intent gotoAbout = new Intent(this,AboutActivity.class);
            startActivity(gotoAbout);
            overridePendingTransition(R.anim.slide_in_from_right, R.anim.slide_out_to_left);
            return true;
        }


        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        Object o = parent.getItemAtPosition(position);
        if (o instanceof CharSequence) {
            String selection = (String) ((CharSequence) o.toString().toLowerCase());    //cast to CharSequence then convert to String

            if (selection.equals("-")) {
                //this is here for clarity, but this selection means that the user hasn't made a selection yet
                //so we do nothing
            } else if (selection.contains("white")) {
                SettingsActivity.colorCode = Color.WHITE;
            } else if (selection.contains("red")) {
                SettingsActivity.colorCode = Color.RED;
            } else if (selection.contains("yellow")) {
                SettingsActivity.colorCode = Color.YELLOW;
            } else if (selection.contains("aqua")) {
                SettingsActivity.colorCode = Color.CYAN;
            } else if (selection.contains("green")) {
                SettingsActivity.colorCode = Color.GREEN;
            } else {
                SettingsActivity.colorCode = Color.WHITE;
                Toast unrecognizedSelection = Toast.makeText(this, "Unsupported Color", Toast.LENGTH_LONG);
                unrecognizedSelection.show();
            }
            updateColors();
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
        return;
    }
}
