package edu.carleton.imhoffc.simpleandroidapp;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.QuickContactBadge;
import android.widget.TextView;


public class FormActivity extends Activity implements TextView.OnEditorActionListener, View.OnClickListener {

    //UI References
    private AutoCompleteTextView animalField;
    private EditText phoneNumberField;
    private EditText nameField;
    private TextView contactTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form);

        //link UI references
        this.animalField = (AutoCompleteTextView)this.findViewById(R.id.animalTextField);
        this.phoneNumberField = (EditText)this.findViewById(R.id.phoneNumberField);
        this.nameField = (EditText)this.findViewById(R.id.nameTextField);
        this.contactTextView = (TextView)this.findViewById(R.id.contactTextView);

        //set up autocomplete for animal field
        String[] animals = getResources().getStringArray(R.array.animals);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, animals);
        animalField.setAdapter(adapter);

        //set up IME actions for each field
        animalField.setImeOptions(EditorInfo.IME_ACTION_NEXT);
        animalField.setOnEditorActionListener(this);
        phoneNumberField.setImeOptions(EditorInfo.IME_ACTION_NEXT);
        phoneNumberField.setOnEditorActionListener(this);
        nameField.setImeOptions(EditorInfo.IME_ACTION_GO);
        nameField.setOnEditorActionListener(this);

        //set up button action
        Button button = (Button)this.findViewById(R.id.button);
        button.setOnClickListener(this);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_form, menu);
        getMenuInflater().inflate(R.menu.menu_home,menu);
        return true;
    }

    //Checks for form errors, and highlights them if they exist.
    //Returns true iff no errors
    private boolean checkForm() {
        boolean allGood = true;
        if (animalField.getText() == null || animalField.getText().toString().equals("")) {
            animalField.setError(getResources().getString(R.string.input_error_noInput));
            animalField.requestFocus();
            allGood = false;
        }
        if (phoneNumberField.getText() == null || phoneNumberField.getText().toString().equals("")) {
            phoneNumberField.setError(getResources().getString(R.string.input_error_noInput));
            phoneNumberField.requestFocus();
            allGood = false;
        }
        if (nameField.getText() == null || nameField.getText().toString().equals("")) {
            nameField.setError(getResources().getString(R.string.input_error_noInput));
            nameField.requestFocus();
            allGood = false;
        }
        return allGood;
    }

    private void hideKeyboard() {
        InputMethodManager inputMethodManager = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(nameField.getWindowToken(), 0);
    }

    private void clearForm() {
        animalField.setText("");
        phoneNumberField.setText("");
        nameField.setText("");
        contactTextView.setText("");
    }

    private void submitForm() {
        String animal = animalField.getText().toString();
        String phoneNumber = phoneNumberField.getText().toString();
        String name = nameField.getText().toString();

        clearForm();

        String contact = "Meet "+name+", the "+animal+". \n Mobile; "+phoneNumber;
        contactTextView.setText(contact);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        // Sliding transition, as said to be used by Jeff
        if (id == android.R.id.home) {
            finish();
            overridePendingTransition(R.anim.slide_in_from_left, R.anim.slide_out_to_right);
            return true;
        } else if (id == R.id.action_clear) {
            clearForm();
            return true;
        } else if (id == R.id.action_about) {
            Intent gotoAbout = new Intent(this,AboutActivity.class);
            startActivity(gotoAbout);
            overridePendingTransition(R.anim.slide_in_from_right, R.anim.slide_out_to_left);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
        if (v.getId() == R.id.animalTextField) {
            if (actionId == EditorInfo.IME_ACTION_NEXT) {
                animalField.clearFocus();
                phoneNumberField.requestFocus();
                return true;
            }
        } else if (v.getId() == R.id.phoneNumberField) {
            if (actionId == EditorInfo.IME_ACTION_NEXT) {
                phoneNumberField.clearFocus();
                nameField.requestFocus();
                return true;
            }
        } else if (v.getId() == R.id.nameTextField) {
            if (actionId == EditorInfo.IME_ACTION_GO) {
                nameField.clearFocus();
                hideKeyboard();
                if (checkForm()) {
                    submitForm();
                }
            }
        }
        return false;
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.button) {
            hideKeyboard();
            if (checkForm()) {
                submitForm();
            }
        }
    }
}
