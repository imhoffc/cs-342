package edu.carleton.imhoffc.simpleandroidapp;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;


public class TransitionsActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transitions);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_transitions, menu);
        getMenuInflater().inflate(R.menu.menu_home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == android.R.id.home) {
            Intent intent = new Intent(this, HomeActivity.class);   //this transition has to be explicit, because TransitionActivities build into more TrasitionActivities
            startActivity(intent);
            overridePendingTransition(R.anim.slide_in_from_left, R.anim.slide_out_to_right);
            return true;
        }

        if (id == R.id.action_bottom) {
            Intent intent = new Intent(this, TransitionsActivity.class);
            startActivity(intent);
            overridePendingTransition(R.anim.slide_in_from_bottom, R.anim.slide_out_to_bottom);
            return true;
        }

        if (id == R.id.action_top) {
            Intent intent = new Intent(this, TransitionsActivity.class);
            startActivity(intent);
            overridePendingTransition(R.anim.slide_in_from_top, R.anim.slide_out_to_top);
            return true;
        }

        if (id == R.id.action_insanity) {
            Intent intent = new Intent(this, TransitionsActivity.class);
            startActivity(intent);
            overridePendingTransition(R.anim.insanity_in, R.anim.insanity_out);
            return true;
        }

        if (id == R.id.action_about) {
            Intent gotoAbout = new Intent(this,AboutActivity.class);
            startActivity(gotoAbout);
            overridePendingTransition(R.anim.slide_in_from_right, R.anim.slide_out_to_left);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

}
