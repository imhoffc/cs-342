package edu.carleton.imhoffc.simpleandroidapp;

import android.app.Activity;
import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Collections;


public class HomeActivity extends ListActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //Set the primary contents of the view
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        //Set up the ActivityListAdapter...
        String[] rawActivities = getResources().getStringArray(R.array.activities); //pull in the list contents
        ArrayList<String> activities = new ArrayList<>(); //create the ArrayList
        Collections.addAll(activities,rawActivities);   //fill the ArrayList
        ActivityListAdapter activityListAdapter = new ActivityListAdapter(this, R.layout.list_cell, activities);

        //...and assign it to the associated view object
        ListView activitiesListView = (ListView)this.findViewById(android.R.id.list);
        activitiesListView.setAdapter(activityListAdapter);
    }

    @Override
    public void onResume() {
        super.onResume();
        ListView list = (ListView)this.findViewById(android.R.id.list);
        list.invalidateViews();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_about) {
            Intent gotoAbout = new Intent(this,AboutActivity.class);
            startActivity(gotoAbout);
            overridePendingTransition(R.anim.slide_in_from_right, R.anim.slide_out_to_left);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onListItemClick(ListView parent, View cell, int position, long id) {
        TextView textView = (TextView) cell.findViewById(R.id.listCellTextView);
        if (textView != null) {
            Intent intent = null;

            //find an activity associated with the value of the cell
            CharSequence key = textView.getText();
            if (key.equals(getResources().getString(R.string.title_activity_about))) {
                intent = new Intent(this, AboutActivity.class);
            } else if (key.equals(getResources().getString(R.string.title_activity_web))) {
                intent = new Intent(this, WebActivity.class);
            } else if (key.equals(getResources().getString(R.string.title_activity_image_sound))) {
                intent = new Intent(this, ImageSoundActivity.class);
            } else if (key.equals(getResources().getString(R.string.title_activity_long_process))) {
                intent = new Intent(this, LongProcessActivity.class);
            } else if (key.equals(getResources().getString(R.string.title_activity_form))) {
                intent = new Intent(this, FormActivity.class);
            } else if (key.equals(getResources().getString(R.string.title_activity_transitions))) {
                intent = new Intent(this, TransitionsActivity.class);
            } else if (key.equals(getResources().getString(R.string.title_activity_settings))) {
                intent = new Intent(this, SettingsActivity.class);
            }

            else {
                Toast defaultPrint = Toast.makeText(this, key, Toast.LENGTH_SHORT);
                defaultPrint.show();
                return;
            }
            startActivity(intent);
            overridePendingTransition(R.anim.slide_in_from_right, R.anim.slide_out_to_left);
        }
    }
}
