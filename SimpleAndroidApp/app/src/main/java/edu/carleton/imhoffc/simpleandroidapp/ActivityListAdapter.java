package edu.carleton.imhoffc.simpleandroidapp;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

public class ActivityListAdapter extends ArrayAdapter<String> {

    public ActivityListAdapter(Context context, int resource, ArrayList<String> listItems) {
        super(context, resource, listItems);
    }

    /*
    /   This method will be called whenever the table is either wishing to create a cell or update one
    /   in it, we create a cell (View) from either a convertView (a recycled one) or if null, we inflate one
    /   from our template 'list_cell.xml' file. We then find our TextView within the cell, and set the text.
    */
    public View getView(int position, View convertView, ViewGroup parent) {
        View cell = convertView;
        if (cell == null) {
            //create the view if needed
            LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            cell = inflater.inflate(R.layout.list_cell,null);
        }
        if (cell != null) {
            //update the view with appropriate content for position
            TextView text = (TextView) cell.findViewById(R.id.listCellTextView); //pull the textView from the cell
            text.setText(this.getItem(position)); //place in the correct data
            text.setTextColor(SettingsActivity.colorCode);
        }
        return cell;
    }
}
