package edu.carleton.imhoffc.simpleandroidapp;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.os.SystemClock;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

public class LongProcessActivity extends Activity implements TextView.OnEditorActionListener, View.OnClickListener {

    //Message Codes
    private final static int MESSAGE_TYPE_PROGRESS = 1;
    private final static int MESSAGE_TYPE_COMPLETED = 2;
    private final static int MESSAGE_TYPE_CANCELLED = 3;
    private WaitHandler handler;
    private Thread currentWaitingThread;

    //UI References
    private TextView resultsTextView;
    private ProgressBar progressBar;
    private EditText input;
    private Button cancelButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_long_process);

        handler = new WaitHandler();

        //set up UI references, these will be used frequently
        resultsTextView = (TextView)this.findViewById(R.id.resultTextView);
        progressBar = (ProgressBar)this.findViewById(R.id.progressBar);
        input = (EditText)this.findViewById(R.id.editText);
        cancelButton = (Button)this.findViewById(R.id.cancelButton);
        cancelButton.setOnClickListener(this);

        //set the editText to run our action
        input.setImeOptions(EditorInfo.IME_ACTION_GO);
        input.setOnEditorActionListener(this);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_long_process, menu);
        getMenuInflater().inflate(R.menu.menu_home,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        // Sliding transition, as said to be used by Jeff
        if (id == android.R.id.home) {
            finish();
            overridePendingTransition(R.anim.slide_in_from_left, R.anim.slide_out_to_right);
            return true;
        } else if (id == R.id.action_about) {
            Intent gotoAbout = new Intent(this,AboutActivity.class);
            startActivity(gotoAbout);
            overridePendingTransition(R.anim.slide_in_from_right, R.anim.slide_out_to_left);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
        if (actionId == EditorInfo.IME_ACTION_GO) {
            hideKeyboard();
            cancelLongProcess();    //we have a new request, so cancel the old one

            //possible input error : no input
            if (input.getText() == null || input.getText().toString().equals("")) {
                input.setError(getResources().getString(R.string.input_error_noInput));
                input.requestFocus();
                return true;
            }

            int time = Integer.parseInt(input.getText().toString());

            //possible input error : invalid value
            if (time <= 0) {
                input.setError(getResources().getString(R.string.input_error_nonPositiveInput));
                input.requestFocus();
                return true;
            }

            this.startLongProcess(time);
            return true;
        }
        return false;
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.cancelButton) {
            cancelLongProcess();
        }
    }

    private void cancelLongProcess() {
        if (this.currentWaitingThread != null) {
            this.currentWaitingThread.interrupt();  //this schedules the thread for safe interruption
            this.currentWaitingThread = null;   //this just abandons our reference to it
        }
    }

    private void hideKeyboard() {
        InputMethodManager inputMethodManager = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(input.getWindowToken(), 0);
    }

    private void startLongProcess(int time) {
        //interrupt our old thread (this is our last chance to this, as our reference to it will be overridden soon)
        cancelLongProcess();

        //reset UI indicators
        resultsTextView.setText(R.string.long_process_results_inProgress1);
        progressBar.setProgress(0);
        progressBar.setMax(time*10);
        cancelButton.setEnabled(true);

        //start process
        currentWaitingThread = new Thread(new WaitRunnable(time));
        currentWaitingThread.setDaemon(true);
        currentWaitingThread.start();
    }


    //Embedded Classes to handle and run actions happening on other threads
    private class WaitRunnable implements Runnable {

        private int maxTime;
        private int timeCounted;
        private int fraction;

        public WaitRunnable(int time) {
            this.maxTime = time;
            this.timeCounted = 0;
        }

        @Override
        public void run() {
            fraction = 0;
            while (timeCounted <= maxTime) {
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    //we've been stopped via Thread.interrupt();
                    handler.sendEmptyMessage(LongProcessActivity.MESSAGE_TYPE_CANCELLED);
                    return;
                }

                Message progress = new Message();
                progress.what = LongProcessActivity.MESSAGE_TYPE_PROGRESS;
                progress.arg1 = timeCounted*10 + fraction;
                handler.sendMessage(progress);
                fraction++;
                if (fraction % 10 == 0) {
                    fraction = 0;
                    timeCounted++;
                }
            }
            handler.sendEmptyMessage(LongProcessActivity.MESSAGE_TYPE_COMPLETED);
        }
    }

    private class WaitHandler extends Handler {
        @Override
        public void handleMessage(Message message) {
            switch (message.what) {
                case LongProcessActivity.MESSAGE_TYPE_PROGRESS:
                    progressBar.setProgress(message.arg1);
                    float percentage = (float)progressBar.getProgress()/(float)progressBar.getMax();
                    if (percentage >= 0.9) {
                        resultsTextView.setText(R.string.long_process_results_inProgress3);
                    } else if (percentage >= 0.5) {
                        resultsTextView.setText(R.string.long_process_results_inProgress2);
                    } else {
                        resultsTextView.setText(R.string.long_process_results_inProgress1);
                    }
                    break;

                case LongProcessActivity.MESSAGE_TYPE_COMPLETED:
                    progressBar.setProgress(progressBar.getMax());
                    resultsTextView.setText(R.string.long_process_results_done);
                    cancelButton.setEnabled(false);
                    break;

                case LongProcessActivity.MESSAGE_TYPE_CANCELLED:
                    progressBar.setProgress(0);
                    resultsTextView.setText(R.string.long_process_results_cancelled);
                    cancelButton.setEnabled(false);
                    break;
            }
        }
    }


}
